# TSPvGA - Traveling Salesman Problem via Genetic Algorithm
# Copyright (C) 2017  Jaime Bemarás
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from cmath import pi, rect
from random import randint


def generate_coordinates(count=10, radius=100, random=True):
    coordinates = ()

    if random:
        while len(coordinates) < count:
            coordinates = tuple(set([
                (randint(-radius, radius), randint(-radius, radius))
                for n in range(count)
            ]))
    else:
        points = [rect(radius, 2 * pi * n / count) for n in range(count)]
        coordinates = tuple([(z.real, z.imag) for z in points])

    return coordinates

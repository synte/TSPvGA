#! /usr/bin/python3

# TSPvGA - Traveling Salesman Problem via Genetic Algorithm
# Copyright (C) 2017  Jaime Bemarás
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from argparse import Action, ArgumentParser
from collections import deque
import concurrent.futures
from copy import deepcopy
from math import factorial, isinf
from pprint import pprint

import Cities
import GeneticOps


def AssessFitness(population, coordinates, Executor):
    unassessed = [individual for individual in population if isinf(individual['fitness'])]

    futures = [
        Executor.submit(GeneticOps.AssessFitness, individual, coordinates)
        for individual in unassessed
    ]

    if len(unassessed) == len(population):
        population.clear()
    else:
        for individual in unassessed:
            if individual in population:
                population.remove(individual)

    for future in concurrent.futures.as_completed(futures):
        population.append(future.result())

    population.sort(key=lambda individual: individual['fitness'])


def FillPopulation(individuals, cities, population, Executor):
    while len(population) < individuals:
        futures = [
            Executor.submit(GeneticOps.NewIndividual, cities)
            for i in range(individuals - len(population))
        ]
        for future in concurrent.futures.as_completed(futures):
            result = future.result()
            if result not in population:
                population.append(result)


def ParseCmdLine():
    class CitiesAction(Action):
        def __init__(self, option_strings, dest, **kwargs):
            super(CitiesAction, self).__init__(option_strings, dest, **kwargs)

        def __call__(self, parser, namespace, values, option_string=None):
            if not values % 2 == 0:
                raise Exception(
                    'To maintain the code simple, -c or --cities MUST specify an even number.'
                )
            else:
                setattr(namespace, self.dest, values)

    parser = ArgumentParser(
                description=(
                    'TSPvGA - '
                    'A solver for the Traveling Salesman Problem via a Genetic Algorithm'
                ),
                epilog="--circle is useful to gain confidence in the "
                       "algorithm's effectivity. "
                       "The resulting fitness should approach (2 * pi) as the "
                       "number of cities and generations increase. "
                       "When --circle is omitted, the cities are scattered randomly."
            )
    parser.add_argument(
        '-c', '--cities',
        help='Number of cities to visit. Should be an even number. (default = 10)',
        metavar='C', type=int, default=10, action=CitiesAction
    )
    parser.add_argument(
        '--circle',
        help='Arrange the cities succesively in a circle of radius = 1. (default = False)',
        action='store_true'
    )
    parser.add_argument(
        '-i', '--individuals', help='Number of individuals per generation. (default = 100)',
        metavar='I', type=int, default=100
    )
    parser.add_argument(
        '-s', '--settlement-generations',
        help='Generations where the fittest individual remains the same'
             ' before deeming that individual as the solution. (default = 25)',
        metavar='G', type=int, default=25
    )

    return parser.parse_args()


def Solve(parameters):
    cities = parameters.cities
    individuals = parameters.individuals
    settlement_generations = parameters.settlement_generations
    radius = 1 if parameters.circle else 100

    print("- Generating coordinates for {} cities:\n".format(cities))
    coordinates = Cities.generate_coordinates(
        random=not parameters.circle, count=cities, radius=radius
    )
    pprint(coordinates)
    print()

    search_space = factorial(cities)
    print('- Search space: {:,} possibilities'.format(search_space))
    print()

    print('- Settlement generations: {}'.format(settlement_generations))
    print('- Population size: {}'.format(individuals))
    print()

    latest_fitness = deque(
        [float('infinity') for i in range(settlement_generations)],
        settlement_generations
    )

    with concurrent.futures.ProcessPoolExecutor() as Executor:
        print('- Generating initial population')
        population = []
        FillPopulation(individuals, cities, population, Executor)

        print('- Evaluating initial population')
        AssessFitness(population, coordinates, Executor)
        latest_fitness.append(population[0]['fitness'])
        print('\t* Fittest Individual in initial population:\n\t', population[0])

        print()
        print('- Evolving:')
        generation = 0
        settled = False
        while not settled:
            generation += 1
            # Select top 25% fittest individuals
            top_candidates = population[1:int(individuals / 4) + 1]
            del population[1:]

            # Cross-breed
            futures = [
                Executor.submit(GeneticOps.CrossBreed, population[0], candidate)
                for candidate in top_candidates
            ]
            for future in concurrent.futures.as_completed(futures):
                for individual in future.result():
                    if individual not in population:
                        population.append(individual)

            # Mutate
            mutants = [GeneticOps.Mutate(individual) for individual in deepcopy(population)]
            population += mutants

            # Refill population with new individuals
            FillPopulation(individuals, cities, population, Executor)

            # Evaluate generation
            AssessFitness(population, coordinates, Executor)
            if generation % settlement_generations == 0:
                print('.', end='', flush=True)

            # Evaluate settlement criteria
            latest_fitness.append(population[0]['fitness'])
            if latest_fitness.count(population[0]['fitness']) == settlement_generations:
                total_individuals = generation * individuals
                print()
                print()
                print('= Settlement criteria reached.')
                print(
                    '\tEvaluated {:,} individuals using {:,} generations'.format(
                        total_individuals, generation
                    )
                )
                print(
                    '\t({:.50%} of an exhaustive search)'.format(total_individuals / search_space)
                )
                print()
                print('\t* Fittest Individual:\n\t', population[0])
                print()
                settled = True


if __name__ == '__main__':
    parameters = ParseCmdLine()
    Solve(parameters)

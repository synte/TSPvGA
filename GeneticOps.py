# TSPvGA - Traveling Salesman Problem via Genetic Algorithm
# Copyright (C) 2017  Jaime Bemarás
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from collections import Counter
from random import randint


def AssessFitness(individual, coordinates):
    """
    Evaluate and set an individual's fitness. Smaller is better.
    """
    individual['fitness'] = 0
    z_a = complex(*coordinates[individual['chromosomes'][0]])

    for chromosome in individual['chromosomes'][1:]:
        z_b = complex(*coordinates[chromosome])
        individual['fitness'] += abs(z_b - z_a)
        z_a = z_b

    return individual


def CrossBreed(ParentA, ParentB):
    """
    Interleave the chromosomes of the provided individuals, producing two
    hybrids.

    The resulting hybrids may have mutations due to duplication of chromosomes
    during the interleavings.
    """
    def RestoreMissingChromosomes(chromosomes):
        """
        Look for repeated chromosomes and restore the missing ones.
        """
        present = set(chromosomes)
        missing = set(range(len(chromosomes))) - present

        duplicates = Counter(chromosomes)
        duplicates.subtract(present)
        duplicates = +duplicates    # Remove non-duplicates. See Counter docs.

        for duplicate in duplicates.elements():
            chromosomes[chromosomes.index(duplicate)] = missing.pop()

    ChildA = {'chromosomes': []}
    ChildB = {'chromosomes': []}

    for pair in zip(ParentA['chromosomes'][0::2], ParentB['chromosomes'][1::2]):
        ChildA['chromosomes'] += list(pair)
    for pair in zip(ParentA['chromosomes'][1::2], ParentB['chromosomes'][0::2]):
        ChildB['chromosomes'] += list(pair)

    RestoreMissingChromosomes(ChildA['chromosomes'])
    RestoreMissingChromosomes(ChildB['chromosomes'])

    ChildA['fitness'] = ChildB['fitness'] = float('infinity')

    return [ChildA, ChildB]


def Mutate(individual):
    """
    Swap two chromosomes from the given individual, and reset its fitness.
    """
    positions = len(individual['chromosomes']) - 1
    x = y = float('infinity')
    while x == y:
        x = randint(0, positions)
        y = randint(0, positions)

    gene = individual['chromosomes'][x]
    individual['chromosomes'][x] = individual['chromosomes'][y]
    individual['chromosomes'][y] = gene

    individual['fitness'] = float('infinity')

    return individual


def NewIndividual(size):
    """
    Generate an individual with its chromosomes in random order.
    """
    dna = [n for n in range(size)]
    chromosomes = [dna.pop(randint(0, x)) for x in range(size - 1, -1, -1)]

    return {'chromosomes': chromosomes, 'fitness': float('infinity')}
